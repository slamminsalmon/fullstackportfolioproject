'use strict';
var express = require('express');
var router = express.Router();
var phraseCount = 0;
var phrases = [];

var readFile = function ()
{
  // read the file

  // parse by new line and longer than 1

  // store phrase in phrases

  // increment phraseCount

}

var genRandPhrase = function ()
{
  // gen random number between zero and phraseCount
  var randIndex = Math.random() * phraseCount;
  // use as index into phrases and return it
  return phrases[randIndex];
}
var getData = function ()
{
  var data =
  {
    'item1': 'https://images.unsplash.com/photo-1563422156298-c778a278f9a5',
    'item2': 'https://images.unsplash.com/photo-1620173834206-c029bf322dba',
    'item3': 'https://images.unsplash.com/photo-1602491673980-73aa38de027a'
  }
  return data;
}

/* GET home page. */
router.get('/', function (req, res)
{
  res.render('index', { title: 'Express', "data": getData() });
});

module.exports = router;
